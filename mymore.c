/* mymorev7.c
This version displays the %age of file displayed
*/

#include <stdio.h>
#include <stdlib.h>
#define	PAGELEN	24
#define	LINELEN	512

void do_more(FILE *);
int  get_input(FILE*);

//add fol three lines
long total_size(FILE*);
long total_lines = 0;
long lines_displayed=0;

int main(int argc , char *argv[])
{
   int i=0;
   if (argc == 1){
         do_more(stdin);
   }
   FILE * fp;
   while(++i < argc){
      fp = fopen(argv[i] , "r");
      if (fp == NULL){
         perror("Can't open file");
         exit (1);
      }
//add fol one line
      total_lines = total_size(fp);
      do_more(fp);
      fclose(fp);
   }  
   return 0;
}

void do_more(FILE *fp)
{
   char buffer[LINELEN];
   int rv;
   int num_of_lines=0;
   FILE* fp_tty = fopen("/dev/tty", "r");
   if (fp_tty == NULL)
      exit(1);
   while (fgets(buffer, LINELEN, fp)){
      fputs(buffer, stdout);
      num_of_lines++;
      lines_displayed++; //for calculating percentage
      if (num_of_lines == PAGELEN){
         rv = get_input(fp_tty);
         if (rv == 0){
            printf("\033[1A\033[2K\033[1G");
            break;//
         }
         else if (rv == 1){//user pressed space bar
            num_of_lines -= PAGELEN;
            printf("\033[1A\033[2K\033[1G");  
         }
         else if (rv == 2){//user pressed return/enter
	      printf("\033[1A\033[2K \033[1G");
              num_of_lines -= 1; //show one more line            
         }
         else if (rv == 3){ //invalid character
            printf("\033[1A\033[2K\033[1G");
            break; 
        }
      }
  }
}

/* print message, wait for response, return # of lines to advance q means no, space means yes, CR means one line */
int get_input(FILE * cmdstream)
{
   int c;
   int percentage = 100*lines_displayed/total_lines;
   printf("\033[7m --More-- (%d%%) \033[m", percentage);
   printf("hello");		
   while((c=getc(cmdstream)) != EOF){
      if(c == 'q')
	 return 0;
      if ( c == ' ' )			
	 return 1;
      if ( c == '\n' )	
	 return 2;	
      return 3;
   }
   return 0;
}


long total_size(FILE* fp){
	long lines = 0;
	char buff[LINELEN];
	while(fgets(buff,LINELEN, fp))
		lines++;
	fseek(fp, 0, SEEK_SET);
	return lines;
}
